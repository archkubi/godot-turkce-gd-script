extends Node

# Şimdi sırada sözlükler ; Dizilere benzerler,
# 	Daha iyi anlamak için diziler ve sözlükleri karşılaştırıyoruz ;

var my_array := ["Wizard", 100, 20]

var my_dict := {
	name = "Wizard",
	health = 100,
	offense = 20,
}

# Dizide, karakterin tüm bilgilerine sahibiz, ancak okunaklı değil
#		öğelerin gerçekte neyi temsil ettiğini anlayamıyoruz .

# Ancak, sözlükler anahtar/değer çiftlerini kullandığından, hemen
#		değerlerin her birinin neyi temsil ettiğini anlayabiliriz.

# Bakalım ikisi de çıktıda nasıl gözükecek;

func _ready() -> void:
	print(my_array) # çıktı: [Wizard, 100, 20]
	print(my_dict)	# çıktı: {health:100, name:Wizard, offense:20}

# Basılı sözlükler incelendiğinde anlaşılabilir olsa da, anahtar/değer çiftlerini düzensiz yazdırdı.
# Sözlükler, kelimelerine göre (başlangıçta oluşturulan sırayla değil) alfabetik sırayla yazdırılır.
# Bunu, bir sonraki derste özel bir sözlük yazdırma işlevi oluşturarak çözeceğiz.
# Ama önce, bu konuda bize yardımcı olacak Dictionary sınıfındaki birkaç fonksiyona bakalım.

# ------------- OUTPUT -------------
#	[Wizard, 100, 20]
#	{health:100, name:Wizard, offense:20}
# ----------------------------------
