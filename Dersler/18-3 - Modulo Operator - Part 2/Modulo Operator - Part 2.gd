extends Node

# İşte modulo operatörü için bazı pratik kullanımlar:

func is_number_even(number: int) -> bool:
	var remainder: int = number % 2
	return (remainder == 0)		# 'sayı' 2'ye tam bölünebiliyorsa kalan 0 olur

func is_perfectly_divisible(a: int, b: int) -> bool:
	var remainder: int = a % b
	return (remainder == 0)		#'a', 'b' ile eşit olarak bölünebiliyorsa, kalan 0 olacaktır.

const LETTERS := ["A", "B", "C"]
var index := 0

func print_next_letter() -> void:
	print(LETTERS[index])
	index += 1 # Sarılmamış dizin
	index = (index % LETTERS.size()) # Sarılmış dizin. 3 elemanlı dizi için dizini 0-2 arasında tutar

func _ready() -> void:
	print(is_number_even(3))				# False
	print(is_number_even(4))				# True
	print(is_perfectly_divisible(10, 3))	# False
	print(is_perfectly_divisible(10, 2))	# True
	print_next_letter()						# A
	print_next_letter()						# B
	print_next_letter()						# C
	print_next_letter()						# A
