extends Node
# Çağrıldığında bir fonksiyona veri 'işlemek' mümkündür.
# FOnksiyon daha sonra aldığı verilerle bir şeyler yapabilir.

# Bir fonksiyona 'passed yani , iletilen' verilere 'argument yani , argüman' denir.
# Fonksiyonlar, kendisine herhangi bir argüman iletilmezse kullanılacak olan varsayılan bir argüman değeri atayabilir. 


func sayi_karesi_yazdir(sayi): # Bir tamsayı değişkeni gerektirir
	print(sayi * sayi)

func varsayilan_yada_sayi_karesi_yazdir(sayi = 0): # 'sayı' varsayılan olarak 0 değeri atadı
	# Bu işleve bir argüman iletilmediği sürece 'sayı' 0 olacaktır
	print(sayi * sayi)

func _ready():
	sayi_karesi_yazdir(2)
	varsayilan_yada_sayi_karesi_yazdir() # ( veri hakkında ) Tartışma geçmedi. Varsayılan bağımsız değişkeni (0) kullanacaktır. 
	varsayilan_yada_sayi_karesi_yazdir(5) # Tartışma geçti. Varsayılan argümanı geçersiz kılar.

# ------------- OUTPUT -------------
#	4
#	0
#	25
# ----------------------------------
