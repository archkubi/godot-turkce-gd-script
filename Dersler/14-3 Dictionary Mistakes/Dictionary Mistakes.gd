extends Node

# Sözlükte bir değer belirlediğimizde, henüz sözlükte olmayan bir anahtar kullanırsak
# sözlük, yeni bir anahtar/değer çifti oluşturulacak.

# Sözlüğe yeni bir giriş eklemeye çalışıyorsak, istediğimiz davranış budur.
# Ancak, bunu kazara yapmak kolaydır, bu da bir hataya neden olabilir.
# Sözlük anahtarlarında yazım hataları ve yanlış büyük harf kullanımı, dikkat edilmesi gereken 2 olası hatadır.
# Neyse ki, otomatik tamamlamanın bize mevcut sözlük anahtarlarını vermesine izin vererek bu sorunu önleyebiliriz.
const KEY := {
	name = "name",
	health = "health",
	level = "level"
}

var char_data := {
	KEY.name: "Wizard",
	KEY.health: 100,
	KEY.level: 9,
}

func _ready() -> void:
	print(char_data)			# {health:100, level:9, name:Wizard}
	char_data.health = 200		# (mevcut bir değeri ayarlama)
	print(char_data)			# {health:200, level:9, name:Wizard}
	char_data.Health = 300		# (yanlışlıkla yeni bir sözlük girişi oluşturdu, "Health": 300)
	print(char_data)			# {Health:300, health:200, level:9, name:Wizard}
								# (şimdi "health" ve "Health" anahtarına sahibiz)

# Otomatik tamamlamayı kullanmanın yanı sıra, bu hatayı önlemek için 'assertions' (iddialar) içeren işlevler oluşturabiliriz.
# Önce iddiaların ne olduğunu öğrenelim, sonra sözlük değerlerimizi güvenle ayarlayan bir fonksiyon yapabiliriz.
