extends Node

# Belirli dizi öğelerine erişmek için dizinleri nasıl kullanabileceğimizi zaten gördünüz. Örneğin:

var my_letters := ["A", "B", "C"]
var a: String = my_letters[0]	# A
var b: String = my_letters[1]	# B
var c: String = my_letters[2]	# C

# Bir sözlükteki değerlere erişmenin 3 yolu var. Bunlar:
#		1) köşeli parantez içerisinde sözlük anahtarını kullanın
#		2) bir ":" ve ardından "=" tuşu kullanın
#		3) sözlükte dizin değerini kullanmak için alıyoruz  

var char_info := {
	name = "Wizard",
	health = 100,
	level = 9,
}

var char_name: String = char_info["name"]		# method 1
var char_health: int = char_info.health			# method 2
var char_level: int = char_info.values()[2]		# method 3

# Yöntem 3, bir sonraki derste yapacağımız sözlük türünde dolaşırken kullanışlıdır.

func _ready() -> void:
	print(char_name)	# Wizard
	print(char_health)	# 100
	print(char_level)	# 9
