extends Node

# -------- Constants (const) değiştirilemeyen değerlerdir  --------

# Godot adlandırma kuralı:
#		- const --> Sabitler büyük harflerle adlandırılır 
	#not: bazen const kullanırken küçük harf görebilirsiniz youtube da çok gördüm bunu yapan
#		- var --> Değişkenler küçük harflerle adlandırılır örnek başlangıç harfini büyük yapma
#  , değişkenleri yan yana yazmak içindir

const ISIM = "Kubi"
const ID = 1
const DOGUMGUNU = "1 Ocak , 1901"


func _ready():
	print("isim: ", ISIM)
	print("ID: ", ID)
	print("Dogum Gunu: ", DOGUMGUNU)

#	(Bir sabitin (aşağıdaki gibi) değerini değiştirmeye çalışmak bize hata verecektir.)
#	ID = 2

# ------------- OUTPUT -------------
#	isim : Kubi
#	ID: 1
#	Dogum Gunu: 1 Ocak , 1901
# ----------------------------------

