extends Node

# Artık sözlükle birlikte bir for döngüsü kullanmaya hazırız.

var char_info := {
	name = "Wizard",
	health = 100,
	level = 9,
	offense = 20,
	defense = 7
}

func print_char_info_data_using_a_for_loop() -> void:
	for index in char_info.size(): # .size() sözlüğün boyutunu döndürür (5)
		var key: String = char_info.keys()[index]
		var value = char_info.values()[index] # burada statik yazmayı kullanamayız (değer String veya int olabilir)
		print(key, ": ", value)

# ------------- OUTPUT -------------
#	name: Wizard
#	health: 100
#	level: 9
#	offense: 20
#	defense: 7
# ----------------------------------

func _ready() -> void:
	print_char_info_data_using_a_for_loop()
	print(char_info) # {defense:7, health:100, level:9, name:Wizard, offense:20}

# Sözlüğü 'print(char_info)' kullanarak yazdırsaydık, uzun bir satırda yazdırırdı.
# Bu, küçük sözlükler için uygun olabilir, ancak daha büyük sözlükler için özel bir sözlük yazdırma işlevi kullanmak isteriz.
