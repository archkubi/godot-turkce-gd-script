extends Node

# Artık bir ifadenin doğru olduğunu nasıl iddia edeceğinizi bildiğinize göre,
# bir ifadenin yanlış olduğunu nasıl iddia edeceğinizi bilin.

# Bunu 'değil' sembolünü kullanarak yapabiliriz: ! (Ünlem işareti)
# ! sembolü, önüne koyduğumuz herhangi bir boole değerini reddeder (tersini döndürür).
# (Bir boole 'doğru' veya 'yanlış'tır)

# Bu durumda :
#			!true = false
#			!false = true

# Sembol yerine 'not' kelimesini de kullanabiliriz.
#			not true = false
#			not false = true

# şimdi uygulamaya geçelim
const VALID_WORDS := ["Hello", "Goodbye"]
var word = "Hello"

func _ready() -> void:
	print(VALID_WORDS.has(word))		# True
	print(!VALID_WORDS.has(word))		# False	(Kullanabiliriz ! değeri olumsuzlamak için bir ifadenin önünde)
	print(not VALID_WORDS.has(word))	# False	('!' yerine 'not' kullanabiliriz.)

	assert(VALID_WORDS.has(word))		# Bu iddia geçecek
	assert(VALID_WORDS.has(word), str("Error: ", word))		# Bu iddia başarısız olacak
# assert(not VALID_WORDS.has(word)) # Bu onaylama başarısız olacak
