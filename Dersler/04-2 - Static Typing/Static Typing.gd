extends Node

# Godot dili, GDScript, 'dynamically typed' bir dildir. 
# Bu, GDScript'teki değişkenlerin depoladıkları veri türlerini değiştirebileceği anlamına gelir. 

# Bir değişkende saklanan veri türleri DEĞİŞTİRİLEMEZSE, bir programlama dilinin 'statically typed' statik olarak yazılmış olduğu söylenir. 

# Dinamik yazma esnektir, ancak yanlışlıkla bir değişkenin veri türünü değiştirirseniz hatalara neden olabilir.
# GDScript, oluşturulduğunda bir değişkeni statik olarak yazmanıza olanak tanır. 
# Bir değişkenin gelecekte ne tür verileri depolaması gerektiğini bilmiyorsanız, statik yazma kullanılmalıdır. 

var dinamikDeger = 1 # Bir tamsayı olarak başlatıldı, ancak yine de veri türünü değiştirebilir. 
var statikDeger: int = 2 # Bir tamsayı olarak başlatıldı ve veri türü DEĞİŞTİRİLEMEZ. 

func _ready():
	dinamikDeger = 0.54 # 'my_dynamic_var' artık bir float
	dinamikDeger = "hello" # 'my_dynamic_var' artık bir string
	dinamikDeger = Color.blue # 'my_dynamic_var' artık bir renk

	statikDeger = 10 # BAŞARILI. Aynı veri tipinde (int) yeni bir değere atanabilir 
	statikDeger = 9.9 # BAŞARILI. Float otomatik olarak int'e (9) dönüştürülür (Godot bir uyarı gösterecektir) 
	
#	statikDeger = "goodbye" # Hata.
#	statikDeger = Color.green # Hata.
#	statikDeger = [1, 2, 3] # Hata.
