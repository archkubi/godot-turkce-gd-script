extends Node
# We can create a function which safely adds a NEW entry to our dictionary by copying
#		the previous function we just made and simply putting a '!' (not) in our assertion.

const KEY := {
	name = "name",
	health = "health",
	level = "level",
	offense = "offense" # Bu yeni diziyi 'add_char_data_value' fonksiyonumuzla kullanacağız.
}

var char_data := {
	KEY.name: "Wizard",
	KEY.health: 100,
	KEY.level: 9
	# 'offense' anahtarı bilerek atlandı. 'add_char_data_value()' ile ekleyeceğiz
}

func set_char_data_value(key: String, value) -> void:
	assert(char_data.has(key), str("Key not in dict: ", key)) 	# (dict'in zaten bu anahtara sahip olduğunu iddia ederek)
	char_data[key] = value										# MEVCUT bir sözlük girişini değiştirme

func add_char_data_value(key: String, value) -> void:
	# Kullanmak '!' (değil) sembolü (dict'in bu anahtara sahip OLMADIĞINI iddia ederek)
	assert(!char_data.has(key), str("Key already in dict: ", key))
	char_data[key] = value				# YENİ bir sözlük girişi ekleme

func _ready() -> void:
	print(char_data)					# {health:100, level:9, name:Wizard}
	set_char_data_value(KEY.health, 500)
	add_char_data_value(KEY.offense, 20)
	print(char_data)					# {health:500, level:9, name:Wizard, offense:20}

#	set_char_data_value("defense", 12)	# Hata: başarısız iddia (dict'te "savunma"(defence) anahtarı yok)
#	add_char_data_value("offense", 30)	# Hata: başarısız iddia ("suç"(offense) anahtarı zaten dikte)

# ------------- OUTPUT -------------
#	{health:100, level:9, name:Wizard}
#	{health:500, level:9, name:Wizard, offense:20}
# ----------------------------------
