extends Node
# Bir fonksiyona ilettiğimiz verilere 'argument , argüman' denir.
# Bir fonksiyonun almayı beklediği veri tipine 'parameter , parametre' denir.
# Bir fonksiyon oluşturduğumuzda, ona parametreler ve bir dönüş tipi veriyoruz.

# Bir işlevin parametreleri ve dönüş türü topluca bir işlevin 'signature , imzası' olarak adlandırılır. 

#	| function name | function signature |
func get_squared_int(integer: int) -> int: # Bu işlevin bir tamsayı parametresi ve bir tamsayı dönüş türü vardır. 
	return integer * integer

#    |    name    |signature|
func print_message() -> void: # Bu işlevin parametresi yoktur ve bir değer döndürmez.  (void function)
	print("Hello!")
	
	
# Bir işlev parametresi statik olarak yazılmışsa ve farklı bir parametre iletmeye çalışırsanız 
#		Bu fonksiyona veri tipi argümanı eklerseniz, Godot bir hata gösterecektir. 

# Örneğin, bir mesajı iletmeye çalışırsam hata mesajının söyleyeceği şey şudur:
#		String argument to the 'get_squared_int' function:
#	________________________________________________________
#		error: At "get_squared_int()" call, argument 1.
#				The passed argument's type (String) doesn't match the
#						function's expected argument type (int)
#	________________________________________________________

# Godot, "parametre" yerine "beklenen argüman türü" ifadesini kullanır, ancak aynı anlama gelirler. 
# Unutmayın: Bir "parametre", bir fonksiyonun almayı BEKLEDİĞİ verilerdir.
# 					Bir "argüman", GERÇEKTEN aldığı verilerdir. (Bu veriler çağrıldığında kendisine iletilir)
# (Bazen insanlar 'argüman' ve 'parametreyi' birbirinin yerine kullanır, bu yüzden kafanızın karışmasına izin vermeyin.)
