extends Node
# Fonksiyonların argümanları veya dönüş değerleri statik olarak yazılabilir. 
# Bu şekilde, işlevin yalnızca belirtilen türde/türlerde bir değeri kabul etmesine ve/veya döndürmesine izin verilecektir. 

# Dönüş türü, bir ok (->) ve ardından veri türü kullanılarak statik olarak yazılabilir.
# Döndürülen değer belirtilen veri tipiyle eşleşmelidir, yoksa Godot bize bir hata verecektir.

func dondur(argument):
#	Herhangi bir bağımsız değişken türü kabul edilecek ve aynı veri türü döndürülecektir.
	return argument

func statik_deger_dondur(argument) -> int:
#	Herhangi bir bağımsız değişken türü kabul edilecektir, ancak işlev bir tamsayı döndürmelidir ZORUNLU. 
	return 777

func statik_geleni_statik_deger_dondur(argument: String) -> int:
#	YALNIZCA bir String argümanı kabul edilecektir ve fonksiyon bir tamsayı döndürmelidir ZORUNLU.
	return 999

func sayi_karesi_cagir(number: int) -> int:
#	Bağımsız değişken VE döndürülen değer tamsayı olmalıdır. 
	var squared_number: int = (number * number)
	return squared_number

func _ready() -> void:
	print(dondur(11))				# 11
	print(statik_deger_dondur(10))							# 777
#	print(statik_geleni_statik_deger_dondur(9)) # Error
	print(statik_geleni_statik_deger_dondur("a"))				# 999
#	print(sayi_karesi_cagir("b")) # Error
	print(sayi_karesi_cagir(8))											#64
	
# Varsayılan olarak Godot, '_ready()' gibi yerleşik işlevler için dönüş türünü otomatik tamamlamayacaktır. 
# ancak bu özelliği Editor-> Editor Settings -> Completion -> Add Type Hints kısmından değiştirebilirsiniz.
