extends Node
# As you know, arrays store a list of values called 'elements'.
# Each of these elements has an associated 'index'.
# The 'index' tells us each element's offset (distance) from the beginning of the array.

const LETTERS := ["A", "B", "C"]

# Bu dizideki öğeler aşağıdaki dizinlere( indexlere ) sahiptir:
# _________________
#| Element | Index |
#|---------|-------|
#| A       | 0     |
#| B       | 1     |
#| C       | 2     |
# -----------------

# Dizideki ilk eleman "A" Dizesidir.
# "A" dizinin başında olduğu için "A" ile dizinin başlangıcı arasındaki mesafe 0'dır. 
#		(yani uzaklık yok)

# Bu 0 tabanlı indeksleme, bilgisayar belleğinde depolanan değerlerin konumuna referans veren 'pointers , işaretçilerden' gelir.
# 'LETTERS' dizisini oluşturduğumuzda, her bir elemanın değerinin bilgisayarın hafızasına kaydedilmesi gerekiyordu.
# Bilgisayar, bu öğeleri, ilk öğeden başlayarak, yan yana belleğe kaydedecektir.
# Bir işaretçi, İLK dizi öğesinin ("A") kaydedildiği konuma başvuracaktır (işaret edecektir).

# İlk elemanın hafızadaki yerini bilerek, bilgisayara söyleyerek diğer elemanları alabiliriz. 
#		başlangıç noktasından ne kadar uzağa bakılacağı.
# ("B", "A"dan 1 "adım", "C" ise "A"dan 2 "adım" uzaktadır. )

# Artık dizi indekslerinin neden 1 yerine 0'dan başladığını biliyorsunuz.
