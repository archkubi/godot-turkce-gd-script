extends Node
# Sözlükte anahtar/değer çifti yazmanın 2 yolu vardır:
#		1) colon ":" üst üste nokta işareti kullanarak
#		2) equal sign "=" işareti kullanarak 

# Colonlarda , bir String anahtar olarak kullanılıyorsa, tırnak içine alınmalıdır.
# Eşittir işareti kullanırken, Dize anahtarlarında tırnak işareti OLMAMALIDIR

var colon_dict := { # Dize anahtarları tırnak işaretleri içine alınmalıdır
	"name": "Wizard",
	"health": 100,
	3: "example" # not: değişken adları bir sayı ile başlayamaz, ancak harf karakterleri iler yazılabilir
}

var equals_dict := { # Dize anahtarları tırnak içine alınmamalıdır
	name = "Wizard",
	health = 100,
	colon_dict.size(): "example" # size eylemine göre kullanımda burada colon anahtarı kullanılmalıdır .
}

# Eşittir işareti kullanırken, Dize tanımına benzer GÖRÜNÜYOR, ancak değiller.
var health = 500 # bu değişken, equals_dict içinde bir anahtar olarak KULLANILMAZ

func _ready() -> void:
	print(colon_dict) 	# {3:example, health:100, name:Wizard}
	print(equals_dict) 	# {3:example, health:100, name:Wizard}

# Hangi sözdizimini kullanırsanız kullanın, sözlük verileri aynıdır ve iki nokta üst üste ile yazdırılır.

# ------------- OUTPUT -------------
#	{3:example, health:100, name:Wizard}
#	{3:example, health:100, name:Wizard}
# ----------------------------------
