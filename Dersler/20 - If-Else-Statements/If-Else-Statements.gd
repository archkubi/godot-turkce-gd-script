extends Node
# Özetlemek gerekirse, if ifadesinin kod bloğu yalnızca ifadesi doğruysa yürütülür.
# Peki ya if-ifadesi doğruysa X'in, yanlışsa Y'nin olmasını istiyorsak?
# 'else' anahtar kelimesi bunun içindir.

# if-ifademize bir 'else' bloğu koyarak onu bir 'if-else-ifadesi' haline getirebiliriz.
var health := 20
var damage_amount := 8

func damage_player() -> void:
	health -= damage_amount
	print("You took ", damage_amount, " damage.", " (health: ", health, ")")
	
	if health <= 0:
		print("(Dead)")		# bu satır yalnızca if ifadesi TRUE ise yürütülür
	else:
		print("(Alive)")	# bu satır sadece if-ifadesi FALSE ise yürütülür.

func _ready() -> void:
	damage_player()			# 8 damage. (health: 12)
							# (şuan yaşıyorsun)

	damage_player()			# 8 damage. (health: 4)
							# (şuan yaşıyorsun)

	damage_player()			# 8 damage. (health: -4)
							# (Öldün)

#Sonraki derste, nasıl daha karmaşık bir if-ifadesi yapılacağını öğreneceğiz.
