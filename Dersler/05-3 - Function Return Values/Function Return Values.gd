extends Node

# Fonksiyonlar , verileri çağrılan her şeye geri 'return ,yani veri döndürür'. 
# Döndürülen veriler daha sonra bir değişkene kaydedilebilir veya başka bir değişkene aktarılabilir. 
#		Argüman olarak işlev görür.

# Herhangi bir veri döndürmeyen fonksiyonlara 'void' fonksiyonlar denir.
# Veri döndüren işlevlere(fonksiyonlara) 'geçersiz olmayan ( void olmayan )' işlevler denir. 


func void_fonksiyon():
	print("void_fonksiyon girildi. Hiçbir şey döndürmez.")
	
func void_olmayan_fonksiyon():
	print("void_olmayan_fonksiyon girildi. Değer döndürür.")
	return 345 # Return değeri

func _ready():
	var void_fonksiyon_return_degeri = void_fonksiyon() # Bu değişkene bir değer atanmamıştır, bu nedenle değeri 'boş' null olacaktır.
	var void_olmayan_fonksiyon_return_degeri = void_olmayan_fonksiyon()
	print("void_fonksiyon_return_degeri: ", void_fonksiyon_return_degeri)
	print("void_olmayan_fonksiyon_return_degeri: ", void_olmayan_fonksiyon_return_degeri)

# ------------- OUTPUT -------------
#	void_fonksiyon girildi. Hiçbir şey döndürmez.
#	void_olmayan_fonksiyon girildi. Değer döndürür.
#	void_fonksiyon_return_degeri: Null
#	void_olmayan_fonksiyon_return_degeri: 345
# ----------------------------------
	
