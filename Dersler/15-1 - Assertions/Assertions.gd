extends Node

# İddialar (Assertions), kodumuzun tam olarak beklediğimiz şeyi yaptığından emin olmamızı sağlar.
# Yerleşik 'assert()' işlevini kullanarak bir şeyler ters gittiğinde hata oluşturabiliriz.
# Bu durumda, bir hatayı görmek iyidir, çünkü bize sorunu düzeltme şansı verir.
# Bir hata görmediysek, hata fark edilmeden kayıp gidebilir, bu da izini sürmeyi ve düzeltmeyi zorlaştırır.

# assert() işlevi, argüman ifadesinin doğru mu yanlış mı olduğunu kontrol edecektir.
# Eğer ifade true ise kod çalışmaya devam edecektir.
# Eğer ifade yanlış ise Godot programı duraklatacak ve bize bir hata mesajı gösterecektir.

const WORDS := ["Hello", "Goodbye"]

func print_valid_word(word: String) -> void:
#		  |  ifade        |		# Bir ifade (expression), bir değer üretmek için değerlendirilen bir kod bölümüdür.
	assert(WORDS.has(word))		# İfade yanlışsa program durur, doğruysa devam eder
	assert(WORDS.has(word), str("Invalid word: ", word))
	# Debug penceresine yazdırılacak olan assert() işlevine bir hata mesajı argümanı iletebilirsiniz.
	# str(), tüm bağımsız değişkenleri tek bir dizeye dönüştüren ve ardından dizeyi döndüren yerleşik bir işlevdir.
	print(word)

func _ready() -> void:
	print_valid_word("Hello")		# Hello 						(the assert() ifade sonucu true)
	print_valid_word("Goodbye")		# Goodbye						(the assert() ifade sonucu true)
	print_valid_word("Hi")			# (Error: Assertion failed)		(the assert() ifade sonucu false)

# Bir onaylama başarısız olduğunda, Hata Ayıklayıcı açılır, böylece hatanın nerede oluştuğunu görebiliriz.
# Ayrıca, "yığın izleme" adı verilen, hataya yol açan ardışık işlev çağrılarının bir listesi de gösterilecektir.
# Kodumuzun anlık görüntüsünü görmek için "Yığın Çerçeveleri" bölümündeki her satıra tıklayabiliriz
# her işlev çağrısı sırasında.
# En son "yığın çerçevesine" tıklamak, hatayı oluşturan satırı vurgulayacaktır.
# Hataya neden olan şeyi düzelttikten sonra (bu durumda başarısız bir iddia), oyunu tekrar çalıştırabiliriz
# ve artık hata almadığımızdan emin olun.
