extends Node
# Biraz daha yerleşik dizi sınıfı işlevlerine bakalım.
# (Bunları, 'Yardım Ara' açılır penceresinde 'dizi'yi arayarak Array sınıfı belgelerinde bulabilirsiniz)
var my_array := ["A", "B", "C"]

func _ready() -> void:
	print(my_array)						# prints: [A, B, C]
	my_array.erase("B")					# diziden "B" öğesini kaldırır
	print(my_array)						# [A, C]
	my_array.clear()					# diziyi temizler (hiçbir öğe olmadan boş yapar)
	print(my_array)						# prints: []
	print(my_array.empty())				# prints: Doğru (Dizi boş)
	my_array.append("E")				# dizinin arkasına "E" öğesini ekler 			[E]
	my_array.append("F")				# dizinin arkasına "F" öğesini ekler			[E, F]
	my_array.append("G")				# dizinin arkasına "G" öğesini ekler			[E, F, G]
	my_array.insert(0, "D") 			# "D" öğesini 0 dizinine ekler (dizinin önü)	[D, E, F, G]
	print(my_array)						# prints: [D, E, F, G]
	my_array.pop_back()					# dizinin son öğesini kaldırır ve döndürür		[D, E, F]
	my_array.pop_front()				# dizinin ilk öğesini kaldırır ve döndürür		[E, F]
	print(my_array)						# prints: [E, F]
	my_array += ["H", "I", "J", "K"]	# diziye 4 eleman ekler
	print(my_array)						# prints: [E, F, H, I, J, K]
	my_array.invert()					# dizi öğelerinin sırasını tersine çevirir
	print(my_array)						# prints: [K, J, I, H, F, E]
	print(my_array.min())				# prints: E (en küçük öğe değeri)
	print(my_array.max())				# prints: K (en büyük eleman değeri)
	my_array.push_front("A")			# aynıdır: my_array.insert(0, "A")		[A, K, J, I, H, F, E]
	my_array.push_back("B") 			# aynıdır: my_array.append("B")			[A, K, J, I, H, F, E, B]
	my_array += ["C", "C", "C"]			# diziye 3 eleman ekler					[A, K, J, I, H, F, E, B, C, C, C]
	print(my_array)						# prints: 								[A, K, J, I, H, F, E, B, C, C, C]
	my_array.sort()						# dizinin elemanlarını sıralar			[A, B, C, C, C, E, F, H, I, J, K]
	print(my_array)						# prints: 								[A, B, C, C, C, E, F, H, I, J, K]
	print(my_array.slice(0, 4))			# (0-4 dizininden öğeleri yazdırır)		[A, B, C, C, C]
	print(my_array.find("C"))			# (ilk "C" öğesinin dizinini yazdırır)	2
	print(my_array.rfind("C"))			# (son "C" öğesinin dizinini yazdırır )	4
