extends Node
# Programımız boyunca değişkenlerimizi sık sık artırıyor, azaltıyor, çoğaltıyor ve bölüyoruz.

# Bunu yapmak için bu matematik operatörlerini nasıl kullanacağımızı bilmemiz gerekiyor.

# +         (ilave)
# -         (çıkarma)
#	* 		(çarpma işlemi)
#	/		(bölünme)

var health := 20

# Sağlık değişkenimizi artırmak istiyorsak şunları yapabiliriz:
# sağlık = sağlık + 10

# Başka bir deyişle, diyoruz ki:
# (new_health_value) = (current_health_value) + 10

# Ama bu biraz garip görünüyor ve fazla ayrıntılı.
# Matematik operatöründen hemen sonra '=' işaretini yapıştırarak değişkene yeni bir değer atayabiliriz.
# O zaman değişken adını ikinci kez yazmamıza gerek yok.

# Bunun yerine basitçe şunu yapabiliriz:
func _ready() -> void:
	print(health)		# 20
	health += 10		# sağlığı 10 artır
	print(health)		# 30
	health -= 5			# sağlığı 5 azalt
	print(health)		# 25
	health /= 5			# sağlığı 5'e böl
	print(health)		# 5
	health *= 10		# sağlığı 10 ile çarp
	print(health)		# 50

# Aslında bir matematik operatörü daha var: modulo operatörü (%)
# Çok sık kullanmayacağız ama bazı durumlarda faydalı olacaktır.
# Bir sonraki derste işleyeceğiz.
