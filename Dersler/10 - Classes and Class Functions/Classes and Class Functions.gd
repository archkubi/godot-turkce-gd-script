extends Node

# Şimdiye kadar gördüğünüz her veri türü, kendisiyle ilişkilendirilmiş bir 'class, yani sınıf' dosyasına sahiptir. 
# Bu sınıf dosyası temelde bir komut dosyasıdır, zaten defalarca oluşturduklarımız gibi.

# Bu bir komut dosyasıdır.
# Tıpkı scriptlerimiz içinde fonksiyonlar yazdığımız gibi, 
#		class dosyaları, ilişkili veri türünün kullanabileceği işlevlere sahip olabilir.

# Birkaç veri türünü zaten öğrendiniz: int, float, String, Array, Dictionary, bool, Vector2 ve Vector3 
# Bu veri türlerinin her biri, aynı ada sahip bir sınıf dosyasına (komut dosyası , script) sahiptir.

# Array sınıfı işlevlerinin bir dizi değişkeni ile nasıl kullanılacağını görelim.
var my_array := ["A", "B", "C"]

func _ready() -> void:
	print(my_array.size()) # prints: 3			(dizideki eleman sayısı)
	print(my_array.front()) # prints: A			(dizideki ilk eleman)
	print(my_array.back()) # prints: C			(dizideki son eleman)
	print(my_array.has("B")) # prints: True		(dizide "B" değerine sahip bir eleman var mı ? cevabı : VARDIR[true] ) 
	print(my_array.has("D")) # prints: False	(dizide "D" değerine sahip bir öğe YOKTUR)
	print(my_array.count("C")) # prints: 1		(dizide "C" değerine sahip tam olarak 1 eleman var)

# Array değişkeninin adından sonra bir nokta yazarak Array sınıfındaki fonksiyonları çağırabildik,
#		ardından kullanmak istediğimiz işlevin adı gelir. 

# size(), front() ve back() işlevleri herhangi bir argüman kabul etmez.
# has() ve count() işlevlerinin her ikisi de bir argüman gerektiriyordu, bu yüzden onları çağırdığımızda bir String argümanı ilettik.
# Bu (boş olmayan) işlevlerin her biri, daha sonra yazdırdığımız bir değer döndürdü.
