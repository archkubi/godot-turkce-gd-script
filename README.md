# Godot Türkçe GD Script
Türkiyedeki Godot topluluklarının gelişmesi ve Godot ile yeni tanışmış olan geliştirici arkadaşlarımızın daha kolay öğrenebilmesi için bulduğumuz ingilizce kaynakları Türkçe dilinde sizlere sunuyoruz.


## Başlarken
- [1. ADIM] - Öncelikle proje dosyasını indirip herhangi bir X konumuna çıkarıyoruz (Masaüstüne olabilir belgelerim olabilir vs vs.)

- [2. ADIM] - Projeyi godot üzerinden açıp dersleri uygulamalı olarak incelemek için godot oyun motorunun ilk açılış (Project manager ) ekranında Local Projects (Yerel Projeler) sekmesinde sağ menüde bulunan Import (İçeri aktar) düğmesine basıp Araştır&Git (Browse) tuşuna basarak 1. Adımda belirttiğim X konumuna çıkardığınız godot-tuerkce-gd-script klasöründeki project.godot dosyasını seçip AÇ ( Open) tuşuna basınız [veya project.godot dosyasına 2 defa tıklayın] . Daha sonra Import & Edit ( İçeri aktar & Düzenle ) tuşuna basınız.

- [3. ADIM] -  Eğer motor açıldı ise ; godot un sol tarafında bulunan dosya sistemi (FileSystem) ekranında Dersler klasörünü açın. Daha sonra burada öğrenmek istediğiniz konunun klasörünü açın ve ders isminin .gd uzantılı dosyasını açınız .

[NOT] Dersin .gd uzantılı dosyalı projesinde kodları test etmeden önce aynı isimdeki .tscn uzantılı dosyayı açmayı unutmayın

## Emeği Geçenler Listesi

```
# Göktuğ "OIHD"
# Kubilay "ArchKubi"

```
